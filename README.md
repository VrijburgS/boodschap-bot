# Use cases

## Boodschappen
- Voeg boodschappen toe aan lijstje
    - 1 item
    - meerdere items, komma gescheiden
- Verwijder boodschappen uit lijstje
    - 1 item
    - meerdere items, komma gescheiden
- Reset lijstje boodschappen

## Afwassen
- Ontvang bericht wie er moeten afwassen
- Stuur bericht met melding dat diegene afdroogt