#!/usr/bin/env python
# -*- coding: utf-8 -*-

import yaml
import boodschap.body

config = yaml.load(open("config/keys.yml"))


def main():
    bot = boodschap.body.Boodschap(config['API_KEY'])


if __name__ == "__main__":
    main()
