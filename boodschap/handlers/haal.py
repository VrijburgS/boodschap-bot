from boodschap.dbConn import DbConn
from telegram.ext import CommandHandler


def _callback(bot, update, args):
    grocery_list = [s.lower() for s in args]
    result = 0
    with DbConn() as db:
        result = db.add_groceries(grocery_list)

    reply_success = "Boodschappen toegevoegd: %s" % ", ".join(grocery_list)
    reply_failure = "Er ging iets fout, geen boodschappen toegevoegd."

    bot.send_message(chat_id=update.message.chat_id,
                     text=reply_success if result > 0 else reply_failure)


def cmd_haal_handler():
    return CommandHandler(command="haal",
                          callback=_callback,
                          pass_args=True)
