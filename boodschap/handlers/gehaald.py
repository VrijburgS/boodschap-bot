from boodschap.dbConn import DbConn
from telegram.ext import CommandHandler


def _callback(bot, update, args):
    grocery_list = [s.lower() for s in args]
    result = 0
    reply_success = "Boodschappen verwijderd: %s" % ", ".join(grocery_list)
    reply_failure = "Er ging iets fout, geen boodschappen verwijderd."

    with DbConn() as db:
        if grocery_list[0] == "alles":
            result = db.empty_groceries()
            if result == 1:
                reply_success = "Alle boodschappen verwijderd"
        else:
            result = db.del_groceries(grocery_list)

    bot.send_message(chat_id=update.message.chat_id,
                     text=reply_success if result > 0 else reply_failure)


def cmd_gehaald_handler():
    return CommandHandler(command="gehaald",
                          callback=_callback,
                          pass_args=True)
