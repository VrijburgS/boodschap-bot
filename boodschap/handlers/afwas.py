import boodschap.jobs.afwassers
from telegram.ext import CommandHandler


def _callback(bot, update):
    reply = "Vandaag moeten %s de afwas doen" % boodschap.jobs.afwassers.afwassers()
    bot.send_message(chat_id=update.message.chat_id,
                     text=reply)


def cmd_afwas_hander():
    return CommandHandler(command="afwas",
                          callback=_callback)
