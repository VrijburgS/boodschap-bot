from boodschap.dbConn import DbConn
from telegram.ext import CommandHandler


def _callback(bot, update):
    with DbConn() as db:
        result = db.get_groceries()

    if not result:
        reply = "Geen boodschappen gevonden"
    else:
        reply = "Boodschappen:\n%s" % "\n".join([rec.name.rstrip() for rec in result])

    bot.send_message(chat_id=update.message.chat_id,
                     text=reply)


def cmd_lijstje_handler():
    return CommandHandler(command="lijstje",
                          callback=_callback)
