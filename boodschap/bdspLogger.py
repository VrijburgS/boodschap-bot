import logging


class BdspLogger:
    def __init__(self, filename):
        fn = "%s.log" % filename
        logging.basicConfig(filename=fn,
                            format="%(asctime)s - %(levelname)s - %(message)s",
                            datefmt="%m/%d/%Y %H:%M:%S",
                            level=logging.INFO)
        self.logger = logging.getLogger(__name__)

    def debug(self, msg, *args, **kwargs):
        self.logger.debug(msg, args, **kwargs)

    def info(self, msg, *args, **kwargs):
        self.logger.info(msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        self.logger.warning(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        self.logger.error(msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        self.logger.critical(msg, *args, **kwargs)
