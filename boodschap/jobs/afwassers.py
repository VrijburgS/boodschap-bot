import telegram.ext.jobqueue
import boodschap.dbConn
import yaml

config = yaml.load(open('config/keys.yml'))


def afwassers():
    with boodschap.dbConn.DbConn() as db:
        result = db.get_afwassers()
        if type(result).__name__ == 'Record':
            return str(result.afwassers.rstrip())
        else:
            return "Joost en Joost"


def callback_afwassers(bot, job):
    msg = "Vandaag moeten %s de afwas doen" % afwassers()
    bot.send_message(chat_id=config['CHAT_ID'], text=msg)
