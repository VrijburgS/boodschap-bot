import psycopg2
import psycopg2.extras
import yaml
import bdspLogger
import time


class DbConn:
    _connection = None
    _cursor = None
    _config = None
    _log = None

    def __init__(self):
        self._log = bdspLogger.BdspLogger("db")
        self._config = yaml.load(open("config/keys.yml"))

        self._connect()

    def _connect(self):
        try:
            self._connection = psycopg2.connect(dbname=self._config['DB_NAME'],
                                                user=self._config['DB_USER'],
                                                password=self._config['DB_PASSWD'])
            self._cursor = self._connection.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
        except psycopg2.DatabaseError as e:
            self._log.error("Postgresql failed to connect: %s", e)
            raise

    def _log_cursor(self, cursor):
        if not self._log:
            return

        msg = str(cursor.query)

        self._log.debug(msg)

    def _execute(self, query, params=None):
        try:
            if self._log:
                self._cursor.timestamp = time.time()
            self._cursor.execute(query, params)
            if self._log:
                self._log_cursor(self._cursor)
        except Exception as e:
            if self._log:
                self._log.error("execute() failed: " + e.message)
            raise

        return self._cursor

    def _commit(self):
        return self._connection.commit()

    def add_groceries(self, groceries=None):
        if not groceries:
            return 0
        amount_of_values = ",".join(["(%s)" for k in groceries])
        query = "INSERT INTO boodschappen (NAME) VALUES %s;" % amount_of_values
        cur = self._execute(query, groceries)
        rowcount = cur.rowcount
        self._commit()
        return rowcount

    def del_groceries(self, groceries=None):
        """
        Deletes all provided groceries by name
        :param groceries: Array of names
        :return: Amount of rows deleted
        """
        if not groceries:
            return 0
        amount_of_values = ",".join(["%s" for k in groceries])
        query = "DELETE FROM boodschappen WHERE NAME IN (%s);" % amount_of_values
        cur = self._execute(query, groceries)
        rowcount = cur.rowcount
        self._commit()
        return rowcount

    def empty_groceries(self):
        self._execute("TRUNCATE boodschappen RESTART IDENTITY")
        self._commit()
        return 1

    def get_groceries(self):
        """
        Selects all groceries and returns them in an array
        :return: [Record(name='',quantity=0),..]
        """
        query = "SELECT name, quantity FROM boodschappen;"
        cur = self._execute(query)
        return cur.fetchall()

    def get_afwassers(self):
        query = "SELECT afwassers FROM afwas WHERE day ='%s';" % time.strftime("%a")
        cur = self._execute(query)
        return cur.fetchone()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if not isinstance(exc_type, Exception):
            self._log.debug("Committing transaction")
            self._commit()
        else:
            self._log.debug('Rolling back transaction')
            self._connection.rollback()

        self._cursor.close()
