import telegram.ext
import boodschap.bdspLogger
import boodschap.jobs.afwassers
import datetime
from boodschap.handlers import haal, gehaald, lijstje, afwas

log = boodschap.bdspLogger.BdspLogger("boodschap")


class Boodschap:
    def __init__(self, token):
        log.info("Logging started")
        self.updater = telegram.ext.Updater(token)
        log.info("Updater initialised")
        dp = self.updater.dispatcher
        jq = self.updater.job_queue

        dp.add_handler(haal.cmd_haal_handler())
        dp.add_handler(gehaald.cmd_gehaald_handler())
        dp.add_handler(lijstje.cmd_lijstje_handler())
        dp.add_handler(afwas.cmd_afwas_hander())

        dp.add_error_handler(self.error)
        jq.run_daily(callback=boodschap.jobs.afwassers.callback_afwassers,
                     time=datetime.time(18, 15),
                     days=(0, 1, 2, 3, 4, 5))

        self.updater.start_polling()

        self.updater.idle()

    def error(self, bot, update, error):
        log.warning('Update "%s" caused error "%s"', update, error)
